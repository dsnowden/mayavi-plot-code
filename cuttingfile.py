from numpy import *
def cutfile(pathtofile):
    import os
    # splits up path given into useful bits
    (path, filename) = os.path.split(pathtofile)
    (shortname, ext) = os.path.splitext(filename)
    # the size of the relevant space determines the data indexing
    nx = 107
    ny = 85
    nz = ny
    # initializing arrays
    bx = zeros((nx, ny, nz))
    by = bx.copy()
    bz = bx.copy()
    btot = bx.copy()
    curx = bx.copy()
    cury = bx.copy()
    curz = bx.copy()
    efldx = bx.copy()
    efldy = bx.copy()
    efldz = bx.copy()
    with open(pathtofile, "rb") as f:
        for i in range(0, nx):
            for j in range(0, ny):
                for k in range(0, nz):
                    # each line from the file gets turned into a 1d array
                    line = fromstring(f.readline(), sep=' ')
                    bx[i, j, k] = line[0]
                    by[i, j, k] = line[1]
                    bz[i, j, k] = line[2]
                    btot[i, j, k] = sqrt(bx[i, j, k] ** 2 + by[i, j, k] ** 2 + bz[i, j, k] ** 2)
                    curx[i, j, k] = line[3]
                    cury[i, j, k] = line[4]
                    curz[i, j, k] = line[5]
                    efldx[i, j, k] = line[6]
                    efldy[i, j, k] = line[7]
                    efldz[i, j, k] = line[8]
    # this is to make a folder to put the new numpy files in
    desiredpath = os.path.join(path, shortname)
    try:
        os.mkdir(desiredpath)
    except OSError:
        if not os.path.isdir(desiredpath):
            raise
    with open(os.path.join(desiredpath, 'bfile.npy'), 'wb') as f:
        save(f, btot)
    with open(os.path.join(desiredpath, 'bxyz.npy'), 'wb') as f:
        save(f, array([bx, by, bz]))
    with open(os.path.join(desiredpath, 'curxyz.npy'), 'wb') as f:
        save(f, array([curx, cury, curz]))
    with open(os.path.join(desiredpath, 'efldxyz.npy'), 'wb') as f:
        save(f, array([efldx, efldy, efldz]))

# this line takes input from the user if you run the file directly instead of pulling out the defined function
if __name__ == "__main__":
    cutfile(raw_input('Filename or relative path of file to cut: '))