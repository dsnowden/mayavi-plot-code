__author__ = 'Maya'
from numpy import *
from mayavi import mlab
def cut_plane(filefile, trajfile, colorbartitle="Magnetic Field (nT)", envisage=True, widget=False):
    from scipy.ndimage.filters import uniform_filter
    from mayavi.tools.pipeline import scalar_field, scalar_cut_plane, surface
    from mayavi.sources import builtin_surface
    mlab.options.backend = 'envisage'
    nx = 107  # The data array will have a size of (nx,ny,nz)
    ny = 85
    nz = ny
    xtitan = 56  # specifies the location of titan.  This depends on what box I am taking data from.
    ytitan = 43
    ztitan = ytitan
    """ This next part is pretty hideous but for some reason it wouldn't work otherwise. I tried a couple different
    commands that ought to have multiplied all elements by 0.05 a la Matlab code, but no dice. So instead ugliness: """
    [x, y, z] = meshgrid(arange(xtitan * -0.05, 0.05 * (nx - xtitan), 0.05),
                         arange(ytitan * -0.05, 0.05 * (ny - ytitan), 0.05),
                         arange(-0.05 * ztitan, 0.05 * (nz - ztitan), 0.05), indexing='ij')
    with open(filefile, 'rb') as f:
        btot = load(f)
    [trajy, trajx, trajz] = loadtxt(trajfile, unpack=True)
    btotsm = uniform_filter(btot, size=3)  # this is the equivalent of smooth3
    sfield = scalar_field(x, y, z, btotsm)  # this is built into matlab's slice
    rainbow = scalar_cut_plane(sfield, plane_orientation='z_axes')
    colors = mlab.colorbar(object=rainbow, title=colorbartitle)
    mlab.axes(xlabel="TIIS X", ylabel="TIIS Y", zlabel="TIIS Z")
    # this is the part that controls whether the plane can be moved
    if widget:
        rainbow.implicit_plane.widget.enabled = True
    else:
        rainbow.implicit_plane.widget.enabled = False
    traj = mlab.plot3d(trajx, trajy, trajz, color=(1,0,1))
    # code to make sphere render at size of titan
    titan = builtin_surface.BuiltinSurface()
    titan.source = 'sphere'
    titan.data_source.radius = 1.0
    titan.data_source.phi_resolution = 15
    titan.data_source.theta_resolution = 15
    surface(titan)
    mlab.show()

if __name__ == "__main__":
    cut_plane(raw_input('Filename or relative path of array to graph: '), raw_input(
        "Filename or relative path of trajectory data to graph: "))