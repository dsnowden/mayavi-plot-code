from numpy import *
from mayavi import mlab
def cut_plane(filefile, posfile, trajfile, colorbartitle="Magnetic Field (nT)", envisage=True, widget=False):
    from mayavi.tools.pipeline import scalar_field, scalar_cut_plane, surface
    from mayavi.sources import builtin_surface
    mlab.options.backend = 'envisage'
    with open(filefile, 'rb') as f:
        btot = load(f)
    with open(posfile, 'rb') as f:
        [xx, yy, zz] = load(f)
    [trajy, trajx, trajz] = loadtxt(trajfile, unpack=True)
    sfield = scalar_field(xx, yy, zz, btot)  # this is built into matlab's slice
    rainbow = scalar_cut_plane(sfield, plane_orientation='z_axes')
    colors = mlab.colorbar(object=rainbow, title=colorbartitle)
    mlab.axes(xlabel="TIIS X", ylabel="TIIS Y", zlabel="TIIS Z")
    # this is the part that controls whether the plane can be moved
    if widget:
        rainbow.implicit_plane.widget.enabled = True
    else:
        rainbow.implicit_plane.widget.enabled = False
    traj = mlab.plot3d(trajx, trajy, trajz, color=(1,0,1))
    # code to make sphere render at size of titan
    titan = builtin_surface.BuiltinSurface()
    titan.source = 'sphere'
    titan.data_source.radius = 1.0
    titan.data_source.phi_resolution = 15
    titan.data_source.theta_resolution = 15
    surface(titan)
    mlab.show()

if __name__ == "__main__":
    cut_plane(raw_input('Filename or relative path of array to graph: '), raw_input(
        "Filename or relative path of position data to graph: "), raw_input(
        "Filename or relative path of trajectory data to graph: "
    ))