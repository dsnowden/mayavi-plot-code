__author__ = 'Maya'
from numpy import *
from mayavi import mlab
from mayavi.tools.pipeline import vector_field, surface
from mayavi.sources import builtin_surface
mlab.options.backend = 'envisage'
nx = 107  # The data array will have a size of (nx,ny,nz)
ny = 85
nz = ny
xtitan = 56  # specifies the location of titan.  This depends on what box I am taking data from.
ytitan = 43
ztitan = ytitan
""" This next part is pretty hideous but for some reason it wouldn't work otherwise. I tried a couple different
commands that ought to have multiplied all elements by 0.05 a la Matlab code, but no dice. So instead ugliness: """
[x, y, z] = meshgrid(arange(xtitan * -0.05, 0.05 * (nx - xtitan), 0.05),
                     arange(ytitan * -0.05, 0.05 * (ny - ytitan), 0.05),
                     arange(-0.05 * ztitan, 0.05 * (nz - ztitan), 0.05), indexing='ij')
with open('bxyz.npy', 'rb') as f:
    [bx, by, bz] = load(f)
[trajy, trajx, trajz] = loadtxt('T57_long_traj_flf.txt', unpack=True)
field = vector_field(x, y, z, bx, by, bz)
magnitude = mlab.pipeline.extract_vector_norm(field)
field_lines = mlab.pipeline.streamline(magnitude, seedtype='plane',
                                       integration_direction='both', line_width=0.5)
field_lines.stream_tracer.maximum_propagation = 80.
field_lines.seed.widget.enabled = True
mlab.axes(xlabel="TIIS X", ylabel="TIIS Y", zlabel="TIIS Z")
# code to make sphere render at size of titan
titan = builtin_surface.BuiltinSurface()
titan.source = 'sphere'
titan.data_source.radius = 1.0
titan.data_source.phi_resolution = 15
titan.data_source.theta_resolution = 15
surface(titan)
traj = mlab.plot3d(trajx, trajy, trajz, color=(1, 0, 1))
mlab.show()
