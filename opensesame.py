from numpy import *
nx = 107  # The data array will have a size of (nx,ny,nz)
ny = 85
nz = ny
fielddata = loadtxt("fielddata_lobox1.txt")
bx = zeros((nx, ny, nz))
by = bx.copy()
bz = bx.copy()
btot = bx.copy()
curx = bx.copy()
cury = bx.copy()
curz = bx.copy()
efldx = bx.copy()
efldy = bx.copy()
efldz = bx.copy()
n = 0
for i in range(0, nx):
    for j in range(0, ny):
        for k in range(0, nz):
            bx[i, j, k] = fielddata[n, 0]
            by[i, j, k] = fielddata[n, 1]
            bz[i, j, k] = fielddata[n, 2]
            btot[i, j, k] = sqrt(bx[i, j, k] ** 2 + by[i, j, k] ** 2 + bz[i, j, k] ** 2)
            curx[i, j, k] = fielddata[n, 3]
            cury[i, j, k] = fielddata[n, 4]
            curz[i, j, k] = fielddata[n, 5]
            efldx[i, j, k] = fielddata[n, 6]
            efldy[i, j, k] = fielddata[n, 7]
            efldz[i, j, k] = fielddata[n, 8]
            n += 1
#with open('bfile.npy', 'wb') as f:
#    save(f, btot)
with open('bxyz.npy', 'wb') as f:
    save(f, array([bx, by, bz]))