from numpy import *
from mayavi import mlab
from mayavi.tools.pipeline import vector_field, surface
from mayavi.sources import builtin_surface
mlab.options.backend = 'envisage'

titan_radius = 2575000.

filename = raw_input('particle trajectory data filename: ')
with load('interpdata.npz', 'rb') as f:
    bx = f['bx']
    by = f['by']
    bz = f['bz']
with load('dimvectors.npz', 'rb') as f:
    x_dim = f['x_dim']
    y_dim = f['y_dim']
    z_dim = f['z_dim']
xx, yy, zz = meshgrid(x_dim, y_dim, z_dim, indexing='ij')
#[trajy, trajx, trajz] = loadtxt('T57_long_traj_flf.txt', unpack=True)
field = vector_field(xx, yy, zz, bx, by, bz)
magnitude = mlab.pipeline.extract_vector_norm(field)
field_lines = mlab.pipeline.streamline(magnitude, seedtype='plane',
                                       integration_direction='both', line_width=0.5)
field_lines.stream_tracer.maximum_propagation = 80.
field_lines.seed.widget.enabled = True
mlab.axes(xlabel="TIIS X", ylabel="TIIS Y", zlabel="TIIS Z")
# code to make sphere render at size of titan
titan = builtin_surface.BuiltinSurface()
titan.source = 'sphere'
titan.data_source.radius = 1.0
titan.data_source.phi_resolution = 15
titan.data_source.theta_resolution = 15
surface(titan)
#traj = mlab.plot3d(trajx, trajy, trajz, color=(1, 0, 1))

with load(filename, 'rb') as f:
    x = f['x']/titan_radius
    y = f['y']/titan_radius
    z = f['z']/titan_radius

a = mlab.points3d(x[0, :], y[0, :], z[0, :], color=(0,0,0), scale_factor=0.1)
mlab.show()
