__author__ = 'Maya'

from numpy import *
from mayavi import mlab
from mayavi.tools.pipeline import scalar_field, scalar_cut_plane, surface
from mayavi.sources import builtin_surface

"""
Getting your data into an array will almost certainly require consultation of the numpy documentation.
"""
nx = 121
ny = 121
nz = 121
res = 0.05
# Here we're only doing four kinds of data, but if you have more this is more obviously convenient.
def grid_array():
    return zeros((nx, ny, nz))
bx = grid_array()
by = grid_array()
bz = grid_array()
btot = grid_array()
xx = grid_array()
yy = grid_array()
zz = grid_array()
# This part is obviously going to be very different depending on the dimensions/indexing of your data. There are
# interpolation routines and whatnot within scipy/numpy, but here the data's pretty simple/symmetrical.
xstart = -.5*res*(nx-1)
xstop = -1*xstart
ystart = -.5*res*(ny-1)
ystop = -1*ystart
zstart = -.5*res*(nz-1)
zstop = -1*zstart
# these are the dimension vectors that give the coordinates along each dimension.
#x_dim = linspace(xstart, xstop, num=nx)
#y_dim = linspace(ystart, ystop, num=ny)
#z_dim = linspace(zstart, zstop, num=nz)
# This data was neatly ordered; obviously you'll have to unwrap it differently if you need to fill the arrays out of
# order.
with open("magnetic_field_data", "rb") as f:
    for k in range(0, nz):
        for j in range(0, ny):
            for i in range(0, nx):
                # each line from the file gets turned into a 1d array
                line = fromstring(f.readline(), sep=' ')
                xx[i,j,k] = line[0]
                yy[i,j,k] = line[1]
                zz[i,j,k] = line[2]
                bx[i, j, k] = line[3]
                by[i, j, k] = line[4]
                bz[i, j, k] = line[5]
                btot[i, j, k] = sqrt(bx[i, j, k] ** 2 + by[i, j, k] ** 2 + bz[i, j, k] ** 2)
"""
Here we have a couple lines that are worth looking at despite not being immediately relevant to a tutorial written in
one file. If you want to graph the same data multiple times in multiple ways, you may want to save it in a numpy
specific format. savez and savez_compressed allow you to load multiple arrays into a file.

savez_compressed('interpdata', bx=bx, by=by, bz=bz, btot=btot)
savez('dimvectors', x_dim=xdim, y_dim=ydim, z_dim=zdim)

Then to load this you would do something like:
with load('interpdata.npz') as data:
        btot = data[btot]
    with load('dimvectors.npz') as data:
        x_dim = data['x_dim']
        y_dim = data['y_dim']
        z_dim = data['z_dim']

Now these arrays are loaded and ready to be used!
"""
# This line gives you access to the Mayavi GUI. You'll probably want it on at least to begin with, though loading in
# data through the GUI is a nightmare.
mlab.options.backend = 'envisage'
# Do not forget the indexing argument to meshgrid. Do not do it.
#xx, yy, zz = meshgrid(x_dim, y_dim, z_dim, indexing='ij')
sfield = scalar_field(xx, yy, zz, bx)
# Mayavi guesses which data to apply the axes to given their position in the code...
mlab.axes(xlabel="TIIS X", ylabel="TIIS Y", zlabel="TIIS Z")
rainbow = scalar_cut_plane(sfield, plane_orientation='z_axes')
colors = mlab.colorbar(object=rainbow, title='whatever you want the colorbar title to say')
# If you set this to false, it will make the ugly interaction widget disappear.
rainbow.implicit_plane.widget.enabled = True
# this next bit is what creates the sphere in the graph to represent the planet.
titan = builtin_surface.BuiltinSurface()
titan.source = 'sphere'
titan.data_source.radius = 1.0
titan.data_source.phi_resolution = 15
titan.data_source.theta_resolution = 15
surface(titan)
mlab.show()
