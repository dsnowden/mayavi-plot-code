from numpy import *
from scipy.interpolate import griddata
# this is just to get the number of lines in the file; I googled it and this is the fast way.
def bufcount(filename):
    f = open(filename)
    lines = 0
    buf_size = 1024 * 1024
    read_f = f.read # loop optimization
    buf = read_f(buf_size)
    while buf:
        lines += buf.count('\n')
        buf = read_f(buf_size)
    f.close()
    return lines
n = bufcount('magnetic_field_data')
#now to actually read the data
zcoord = 0.
with open('magnetic_field_data', mode='rb') as d:
    points = zeros((n, 3)) #columns are x, y, z, rows are each point
    bx = zeros((n,))
    by = zeros((n,))
    bz = zeros((n,))
    btot = zeros((n,))
    # l is the counter variable to keep track of where we are
    l = 0
    for line in d:
        nums = fromstring(line, sep=' ')
        if nums[0] > -2.5 and nums[0] < 2.5 and nums[1] > -2.5 and nums[1] < 2.5 and nums[2] > zcoord-0.1 and nums[2] < zcoord+0.1:
            # obviously the indexing is for the other setup; I'll change it
            points[l,0] = nums[0]
            points[l,1] = nums[1]
            points[l,2] = nums[2]
            bx[l] = nums[3]
            by[l] = nums[4]
            bz[l] = nums[5]
            btot[l] = sqrt(bx[l]**2 + by[l]**2 + bz[l]**2)
        l += 1
# to get 100 boxes of size 0.05 with the grid on their centers, you have to fiddle with the end points.
# the j just means that it's counting the number of divisions
xx, yy, zz = mgrid[-2.475:2.475:100j, -2.475:2.475:100j, zcoord-0.05:zcoord+0.05:3j]
# here we have the interpolation function. It wouldn't run with a linear method, though that's a lower priority fix.
#bxn = griddata(points, bx, (xx, yy, zz), method='nearest')
#byn = griddata(points, by, (xx, yy, zz), method='nearest')
#bzn = griddata(points, bz, (xx, yy, zz), method='nearest')
btotn = griddata(points, btot, (xx, yy, zz), method='linear')
# and saving.
#with open('bxyzinterp.npy', 'wb') as f:
#    save(f, array([bxn, byn, bzn]))
with open ('interpedshortgrid.npy', 'wb') as f:
    save(f, array([xx, yy, zz]))
with open('btotinterp.npy', 'wb') as f:
    save(f, btotn)