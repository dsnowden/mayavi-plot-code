__author__ = 'Maya'

from numpy import *
from scipy.interpolate import griddata
from scipy.ndimage import map_coordinates as map
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import datetime

"""
Getting your data into an array will almost certainly require consultation of the numpy documentation.
"""
nx = 121
ny = 121
nz = 121
res = 0.05
# Here we're only doing four kinds of data, but if you have more this is more obviously convenient.
def grid_array():
    return zeros((nx, ny, nz))
bx = grid_array()
by = grid_array()
bz = grid_array()
btot = grid_array()
xx = grid_array()
yy = grid_array()
zz = grid_array()

def traj_array():
    return zeros(241)
traj_year = traj_array()
traj_doy = traj_array()
traj_hour = traj_array()
traj_min = traj_array()
traj_sec = traj_array()
traj_datetime = traj_array()

traj_pos = zeros((241,3))
traj_x = zeros(241)
traj_y = traj_array()
traj_z = traj_array()

mag_year = traj_array()
mag_mon  = traj_array()
mag_day = traj_array()
mag_hour = traj_array()
mag_min  = traj_array()
mag_sec  = traj_array()
mag_bx    = traj_array()
mag_by    = traj_array()
mag_bz    = traj_array()
mag_btot  = traj_array()
mag_datetime = traj_array()


# This part is obviously going to be very different depending on the dimensions/indexing of your data. There are
# interpolation routines and whatnot within scipy/numpy, but here the data's pretty simple/symmetrical.
xstart = -.5*res*(nx-1)
xstop = -1*xstart
ystart = -.5*res*(ny-1)
ystop = -1*ystart
zstart = -.5*res*(nz-1)
zstop = -1*zstart
# these are the dimension vectors that give the coordinates along each dimension.
#x_dim = linspace(xstart, xstop, num=nx)
#y_dim = linspace(ystart, ystop, num=ny)
#z_dim = linspace(zstart, zstop, num=nz)
# This data was neatly ordered; obviously you'll have to unwrap it differently if you need to fill the arrays out of
# order.
with open("magnetic_field_data", "rb") as f:
    for k in range(0, nz):
        for j in range(0, ny):
            for i in range(0, nx):
                # each line from the file gets turned into a 1d array
                line = fromstring(f.readline(), sep=' ')
                xx[i, j, k] = line[0]
                yy[i, j, k] = line[1]
                zz[i, j, k] = line[2]
                bx[i, j, k] = line[3]
                by[i, j, k] = line[4]
                bz[i, j, k] = line[5]
                btot[i, j, k] = sqrt(bx[i, j, k] ** 2 + by[i, j, k] ** 2 + bz[i, j, k] ** 2)

with open("T57_mag", "rb") as mag_file:
    for i in range(0,241):
        line = fromstring(mag_file.readline(), sep=' ')
        mag_year[i] =line[0]
        mag_mon[i]  = line[1]
        mag_day[i] = line[2]
        mag_hour[i] = int(line[3])
        mag_min[i]  = int(line[4])
        mag_sec[i]  = int(line[5])
        mag_bx[i]    = line[6]
        mag_by[i]    = line[7]
        mag_bz[i]    = line[8]
	mag_btot[i]  = line[9] 
        mag_datetime[i] = mdates.date2num(datetime.datetime(int(mag_year[i]),int(mag_mon[i]),int(mag_day[i]),
                                                            int(mag_hour[i]),int(mag_min[i]),int(mag_sec[i])))

#mag_time_temp=mdates.date2num(mag_datetime)

print mag_datetime[10]



with open("T57_long_traj", "rb") as traj_file:
    for i in range(0,241):
        line = fromstring(traj_file.readline(), sep=' ')
        traj_year[i] = line[0]
        traj_doy[i]  = line[1]
        traj_hour[i] = line[2]
        traj_min[i]  = line[3]
        traj_sec[i]  = line[4]
        traj_x[i]    = line[5]/res+61
        traj_y[i]    = line[6]/res+61
        traj_z[i]    = line[7]/res+61
        traj_datetime[i] = mdates.date2num(datetime.datetime(int(mag_year[i]),int(mag_mon[i]),int(mag_day[i]),
                                                             int(traj_hour[i]),int(traj_min[i]),int(traj_sec[i])))

traj_bx = map(bx,(traj_x,traj_y,traj_z))
traj_by = map(by,(traj_x,traj_y,traj_z))
traj_bz = map(bz,(traj_x,traj_y,traj_z))
traj_btot = map(btot,(traj_x,traj_y,traj_z))

f, axarr = plt.subplots(4, sharex=True)


axarr[0].xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))

axarr[0].plot_date(mag_datetime,mag_bx,'-')
axarr[0].plot_date(traj_datetime,traj_bx,'-')
axarr[1].plot_date(mag_datetime,mag_by,'-')
axarr[1].plot_date(traj_datetime,traj_by,'-')
axarr[2].plot_date(mag_datetime,mag_bz,'-')
axarr[2].plot_date(traj_datetime,traj_bz,'-')
axarr[3].plot_date(mag_datetime,mag_btot,'-')
axarr[3].plot_date(traj_datetime,traj_btot,'-')

axarr[0].set_ylabel('$B_x$ (nT)')
axarr[1].set_ylabel('$B_y$ (nT)')
axarr[2].set_ylabel('$B_z$ (nT)')
axarr[3].set_ylabel('$B_{tot}$ (nT)')
axarr[3].set_xlabel('Spacecraft Time (UT)')

plt.show()
